module gitlab.com/rackn/logger

go 1.16

require (
	github.com/fatih/color v1.18.0 // indirect
	github.com/hashicorp/go-hclog v1.6.3
	github.com/mattn/go-colorable v0.1.14 // indirect
)
